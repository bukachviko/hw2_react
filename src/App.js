import {useEffect, useState} from "react";
import './App.css';
import Header from "./Components/Header";
import ListOfProducts from "./Components/ListOfProducts";
import Modal from "./Components/Modal";

let cartItems = JSON.parse(localStorage.getItem('cart'));
if(cartItems === null) {
    cartItems = [];
}

let selectedItems = JSON.parse(localStorage.getItem('selected'));
if (selectedItems === null) {
    selectedItems = [];
}

function App() {
    const [countCartItems, updateCountCartItems] = useState(cartItems.length);

    const [countSelectedItems, updateCountSelectedItems] = useState(selectedItems.length);
    const [isShowModal, setIsShowModal] = useState(false );

    const toggleModal = () => {
        setIsShowModal(!isShowModal)
    }

    const addToCart = (item) => {
        cartItems.push(item)

        localStorage.setItem('cart', JSON.stringify(cartItems))

        updateCountCartItems(cartItems.length)
    }

    const addToSelected = (newItem) => {
    let existItem = selectedItems.filter((item) => item.id === newItem.id)
        if(existItem.length) {
            selectedItems = selectedItems.filter((item) => item.id !== newItem.id)
        } else {
            selectedItems.push(newItem)
        }

        localStorage.setItem('selected', JSON.stringify(selectedItems))

        updateCountSelectedItems(selectedItems.length)
    }

    const isSelectedItem = (id) => {
        return selectedItems.filter((item) => item.id === id).length > 0
    }

    return(
        <div className="wrapper">
            <Header countCartItems={countCartItems} countSelectedItems={countSelectedItems} />

            <ListOfProducts
                setIsShowModal={setIsShowModal}
                addToCart={addToCart}
                addToSelected={addToSelected}
                isSelectedItem={isSelectedItem}
            />

            {isShowModal &&  <Modal
                addToCart={addToCart}
                backgroundColor={{backgroundColor: '#7f9a74'}}
                header={"Додати товар в кошик?"}
                text={"Можливе замовлення від 1 шт"}
                boolean={true}
                nameBtn1={'Добре!'}
                nameBtn2={'Ні!'}
                toggleModal={toggleModal}
            />}
        </div>
    )
}

export default App;
