import React, {useEffect, useState} from 'react';
import ProductCard from "./ProductCard";

function ListOfProducts(props) {

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);

    useEffect(() => {
        fetch("/db.json")
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setItems(result);
                },

                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <ul className="card__container">
                {items.map(item => (
                    <li
                        className="card"
                        key={item.id}>
                        <ProductCard
                            isSelectedItem={props.isSelectedItem}
                            addToCart={props.addToCart}
                            addToSelected={props.addToSelected}
                            setIsShowModal={props.setIsShowModal}
                            isShowModal={props.isShowModal}
                            image={item.image}
                            name={item.name}
                            color={item.color}
                            cost={item.cost}
                            article={item.article}
                            productId={item.id}
                        />
                    </li>
                ))}
            </ul>
        );
    }
}

export default ListOfProducts;