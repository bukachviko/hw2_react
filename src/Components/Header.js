import React from 'react';
import Cart from "./Cart";
import Selected from "./Selected";

function Header({countCartItems, countSelectedItems}) {
    return (
        <div className="header">
            <nav className="navbar">
                <img className="logo__img" src="/logo1.jpg" width="120" height="100" alt=""/>
                <h1 className="navbar__title">"Organic fruit store"</h1>
                <Cart countCartItems={countCartItems} />
                <Selected countSelectedItems={countSelectedItems}/>
            </nav>
        </div>
    );
}

export default Header;
