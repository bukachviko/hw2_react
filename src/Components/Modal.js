import React, {useState} from 'react';
import Button from "./Button";
import "./Modal.css";


export default function Modal({addToCart, backgroundColor, header, text: mainText, nameBtn1, nameBtn2, toggleModal}) {

    return (

        <div className="modal">
            <div onClick={toggleModal}
                 className="overlay"></div>
            <div className="modal-content" style={backgroundColor}>
                <div className="header__modal">
                    <h1 className="header__modal-title">{header}</h1>
                    <Button
                        btnClass="modal__headerCloseBtn"
                        myClick={toggleModal}
                        text={'Х'}
                    />
                </div>
                <p className="modal__text">{mainText}</p>
                <div className="modal__footer">
                    <Button
                        btnClass="add__product-bnt"
                        text={nameBtn1}
                        myClick={() => {
                            toggleModal()
                            addToCart({id: 1})
                        }}
                    />

                    <Button
                        text={nameBtn2}
                        myClick={toggleModal}
                    />
                </div>
            </div>

        </div>

    );
}