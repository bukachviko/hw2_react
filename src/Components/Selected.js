import React from "react";
import { FaHeart } from "react-icons/fa";

const Selected = ({countSelectedItems}) => {
    return (
        <>
            <div className="navbar__selected">
                <div><FaHeart /></div>
                {countSelectedItems > 0 && <div className="navbar__selected-count">{countSelectedItems}</div>}
            </div>
        </>
    )
}

export default Selected;