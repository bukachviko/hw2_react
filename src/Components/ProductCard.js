import Button from "./Button";
import {useState} from "react";
import { HiStar} from "react-icons/hi2";
import { HiOutlineStar } from "react-icons/hi2";


function ProductCard(props) {
    const [isSelected, setIsSelected] = useState(props.isSelectedItem(props.productId));

    return (
        <div>
            <div>
                <img className="card__image-product" width="150" height="150" src={props.image} alt=""/>
                <h3 className="card__title-product">{props.name}</h3>
                <p className="card__color-product">{props.color}</p>
                <p className="card__price-product"> ${props.cost}</p>
                <p className="card__article-product">{props.article}</p>
                <div className="actions">
                    <Button
                        key={1}
                        btnClass="openModalBtn"
                        backgroundColor={{backgroundColor: '#7f9a74'}}
                        text={"Add to cart"}
                        myClick={props.setIsShowModal}
                    />
                    <div className="actions actions__selected"
                         onClick={() => {
                             props.addToSelected({id: props.productId, name: props.name})
                             setIsSelected(props.isSelectedItem(props.productId))
                         }
                         }>
                        {isSelected ? <HiStar fill='#7f9a74'/> : <HiOutlineStar />}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ProductCard;